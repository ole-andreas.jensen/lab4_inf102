package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuickMedian implements IMedian {

    private Random rnd = new Random();

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list

        int medianIndex = listCopy.size() / 2;
        sort(listCopy, medianIndex);
        return listCopy.get(medianIndex);
    }

    private <T extends Comparable<T>> void sort(List<T> list, int medianIndex) {
        T turner = list.get(rnd.nextInt(list.size()));
        List<T> first = new ArrayList<>();
        List<T> turned = new ArrayList<>();
        List<T> last = new ArrayList<>();
        
        for (T t : list) {
            int cmp = t.compareTo(turner);
            if (cmp < 0) {
                first.add(t);
            } else if (cmp > 0) {
                last.add(t);
            } else {
                turned.add(t);
        }
    }

        list.clear();
        list.addAll(first);
        list.addAll(turned);
        list.addAll(last);

        if(list.get(medianIndex).compareTo(turner)==0)
    {
        return;
    }
        if(first.size()>medianIndex) {
        sort(first, medianIndex);
        list.clear();
        list.addAll(first);
    }
        else
    {
        sort(last, medianIndex - first.size() - turned.size());
        list.clear();
        list.addAll(first);
        list.addAll(turned);
        list.addAll(last);
    }
}
}
